package alphacivsrc.domain;
import static org.junit.Assert.*;

import org.junit.Test;

public class TestLinearAging {
	@Test
	public void shouldAge100YearsPerRound(){
		LinearAgingImpl aging = new LinearAgingImpl();
		int age = -4000;
		assertEquals("Should age 100 years to -3900", -3900, aging.calculateAge(age));
	}
}
