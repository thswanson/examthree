package alphacivsrc.domain;

import static org.junit.Assert.*;

import thirdparty.*;
import thirdparty.ThirdPartyFractalGenerator;

import org.junit.Before;
import org.junit.Test;

public class TestFractalAdapter {
	FractalAdapter generator;
	
	/*@Before
	public void setup() {
		generator = new FractalAdapter(new ThirdPartyFractalGenerator());
	}*/
	@Test
	public void testAllTerrains() {
		//ThirdPartyFractalGenerator testTerrain = new ThirdPartyFractalGenerator();
		generator = new FractalAdapter();//(testTerrain);
		ThirdPartyFractalGenerator testTerrain = generator.getFractalThird();
		//TileImpl test = generator.terrainToPlace(new Position(3,0));
		
		for(int i = 0 ; i < 16 ; i ++ ) {
			for(int j = 0 ; j < 16 ; j ++ ) {
				String terrain;
				switch(testTerrain.getLandscapeAt(i, j)) {
				case '.': 
					terrain = GameConstants.OCEANS;
					break;
				case 'o':
					terrain = GameConstants.PLAINS;
					break;
				case 'f':
					terrain = GameConstants.FOREST;
					break;
				case 'h':
					terrain = GameConstants.HILLS;
					break;
				case 'M':
					terrain = GameConstants.MOUNTAINS;
					break;
				default:
					terrain = "Invalid";
					break;
				}
				System.out.println(terrain);
				System.out.println(generator.terrainToPlace(new Position(i,j)).getTypeString());
				
				assertTrue(generator.terrainToPlace(new Position(i,j)).getTypeString().equals(terrain));
			}
		}
		
		/*TileImpl test1 = generator.terrainToPlace(new Position(5,3));
		TileImpl test2 = generator.terrainToPlace(new Position(7,2));
		TileImpl test3 = generator.terrainToPlace(new Position(10,4));
		TileImpl test4 = generator.terrainToPlace(new Position(12,10));
		TileImpl test5 = generator.terrainToPlace(new Position(13,4));
		TileImpl test6 = generator.terrainToPlace(new Position(8,9));
		TileImpl test7 = generator.terrainToPlace(new Position(15,15));
		System.out.println(test.getTypeString());
		System.out.println(test1.getTypeString());
		System.out.println(test2.getTypeString());
		System.out.println(test3.getTypeString());
		System.out.println(test4.getTypeString());
		System.out.println(test5.getTypeString());
		System.out.println(test6.getTypeString());
		System.out.println(test7.getTypeString());
		assertTrue(test1.getTypeString().equals(GameConstants.PLAINS));
		assertTrue(test2.getTypeString().equals(GameConstants.PLAINS));
		assertTrue(test3.getTypeString().equals(GameConstants.PLAINS));
		assertTrue(test4.getTypeString().equals(GameConstants.PLAINS));
		assertTrue(test5.getTypeString().equals(GameConstants.PLAINS));
		assertTrue(test6.getTypeString().equals(GameConstants.PLAINS));
		assertTrue(test7.getTypeString().equals(GameConstants.PLAINS));
		System.out.println(test.getTypeString());
		System.out.println(test1.getTypeString());
		System.out.println(test2.getTypeString());
		System.out.println(test3.getTypeString());
		System.out.println(test4.getTypeString());
		System.out.println(test5.getTypeString());
		System.out.println(test6.getTypeString());
		System.out.println(test7.getTypeString());*/
	}

}
