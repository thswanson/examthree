package alphacivsrc.domain;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
public class TranscriptIntegrationTesting {
	private Game game;
	private TileImpl[][] boardArray;
	private Game logDecorator;
	SwitchView switchView;
	@Before
	public void setUp() {
		game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new DeltaCivSetUp(), new UnitCreationAlphaCiv());
		logDecorator = new LogDecorator((GameImpl)game);
		boardArray = ((GameImpl) game).getBoardArray();
	}
	
	@Test
	public void testUnitMovement() {
		//game.endOfTurn();
		logDecorator.moveUnit(new Position (3,8), new Position(2,8));
		Transcriber transcript =  ((LogDecorator) logDecorator).getTranscript();
		//System.out.println(transcript.transcriptOut());
		String test = transcript.transcriptOut();
		TileImpl [][] board = ((GameImpl)game).getBoardArray();
		assertEquals(GameConstants.ARCHER, (game.getUnitAt(new Position(2,8)).getTypeString()));
	}
	
	@Test
	public void testUnitActionLog() {
		//UnitImpl unit = (UnitImpl)game.getUnitAt(new Position(3,8));
		logDecorator.performUnitActionAt(new Position(3,8));
		Transcriber transcript =  ((LogDecorator) logDecorator).getTranscript();
		//System.out.println(transcript.transcriptOut());
		String test = transcript.transcriptOut();
		//System.out.println(test);
		assertTrue(test.equals("RED archer nothing at 3,8\n"));
	}
	
	@Test 
	public void testChangeFocus() {
		CityImpl city = (CityImpl) logDecorator.getCityAt(new Position(4,5));
		logDecorator.changeWorkForceFocusInCityAt(new Position(4,5), GameConstants.foodFocus);
		Transcriber transcript =  ((LogDecorator) logDecorator).getTranscript();
		//System.out.println(transcript.transcriptOut());
		String test = transcript.transcriptOut();
		//System.out.println(test);
		assertTrue(test.equals("RED changes work focus in city at 4,5 to apple\n"));
	}
	
	@Test
	public void testEndOfTurn() {
		logDecorator.endOfTurn();
		logDecorator.endOfTurn();
		Transcriber transcript =  ((LogDecorator) logDecorator).getTranscript();
		
		String test = transcript.transcriptOut();
		//System.out.println(test);
		assertEquals(test,"RED ended the turn\n"
				+ "BLUE ended the turn\n");
		
	}
	
	@Test
	public void testChangeProduction() {
		logDecorator.changeProductionInCityAt(new Position(4,5), GameConstants.LEGION);
		Transcriber transcript =  ((LogDecorator) logDecorator).getTranscript();
		String test = transcript.transcriptOut();
		assertEquals(test, "RED changes production focus in city at 4,5 to legion\n");
		
	} 
}
