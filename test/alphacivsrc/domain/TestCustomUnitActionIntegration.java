package alphacivsrc.domain;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
public class TestCustomUnitActionIntegration {
	private GameImpl game;
	private TileImpl[][] boardArray;
	@Before
	public void setUp() {
		game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new DeltaCivSetUp(), new CustomUnitCreation());
		//game.setTranscript(new CivTranscriptBuilder());
		boardArray = ((GameImpl) game).getBoardArray();
	}
	
	@Test
	public void testChangingUnitAction() {
		
		UnitImpl archer = (UnitImpl) game.getUnitCreationFactory().createUnit(Player.RED, GameConstants.ARCHER);
		CustomUnitCreation customUnitCreation = (CustomUnitCreation) game.getUnitCreationFactory();
		assertTrue(archer.getUnitAction().getActionName().equals(customUnitCreation.getArcherAction().getActionName()));
		customUnitCreation.setUnitAction(GameConstants.SETTLER, new ArcherActionImpl() );
		UnitImpl settler = (UnitImpl) game.getUnitCreationFactory().createUnit(Player.RED, GameConstants.SETTLER);
		assertTrue(settler.getUnitAction().getActionName().equals(customUnitCreation.getArcherAction().getActionName()));
	}
	@Test
	public void testChangingUnitActionTrhoughGame() {
		game.changeUnitAction(GameConstants.ARCHER, new SettlerAction());
		UnitImpl archer = (UnitImpl) game.getUnitCreationFactory().createUnit(Player.RED, GameConstants.ARCHER);
		assertTrue(archer.getUnitAction().getActionName().equals(((CustomUnitCreation) game.getUnitCreationFactory()).getArcherAction().getActionName()));
	}
	
	@Test
	public void changeCurrentUnitsActionsMidGame() {
		game.changeUnitAction(GameConstants.ARCHER, new SettlerAction());
		ArrayList<UnitImpl> units = game.getUnits();
		for (UnitImpl unit : units) {
			if (unit.getTypeString().equals(GameConstants.ARCHER)) {
				assertTrue(unit.getUnitAction().getActionName().equals(((CustomUnitCreation) game.getUnitCreationFactory()).getArcherAction().getActionName()));
			}
		}
		
	}
	@Test
	public void changeCurrentUnitsActionsMidGameCusom() {
		game.changeUnitAction(GameConstants.ARCHER, new CustomUnitActionIntegration());
		ArrayList<UnitImpl> units = game.getUnits();
		for (UnitImpl unit : units) {
			if (unit.getTypeString().equals(GameConstants.ARCHER)) {
				assertTrue(unit.getUnitAction().getActionName().equals(((CustomUnitCreation) game.getUnitCreationFactory()).getArcherAction().getActionName()));
			}
		}
		
	}
	
	@Test
	public void testArcherMakesCity() {
		game.changeUnitAction(GameConstants.ARCHER, new SettlerAction());
		ArrayList<UnitImpl> units = game.getUnits();
		for (UnitImpl unit : units) {
			if (unit.getTypeString().equals(GameConstants.ARCHER)) {
				assertTrue(unit.getUnitAction().getActionName().equals(((CustomUnitCreation) game.getUnitCreationFactory()).getArcherAction().getActionName()));
			}
		}
		game.performUnitActionAt(new Position(3,8));
		assertTrue(game.getCityAt(new Position(3,8)) != null);
		
	}
	
	
}
class CustomUnitActionIntegration implements UnitActions {

	@Override
	public String getActionName() {
		// TODO Auto-generated method stub
		return "brandNewClass";
	}

	@Override
	public void unitPreformsAction(UnitContext unitContext) {
		UnitImpl unit = unitContext.getTile().getUnit();
		unit.setDefensiveStrength(10000);
		
	}
	
}
