package alphacivsrc.domain;

public class LogDecorator implements Game {
	
	
	private Game game;
	private boolean transcriptOn;
	CivTranscriptBuilder transcript;
	SwitchView switchView;
	
	public LogDecorator (GameImpl game) {
		this.game = game;
		this.transcriptOn = transcriptOn;
		transcript = new CivTranscriptBuilder();
		this.switchView = switchView;
	}

	@Override
	public Tile getTileAt(Position p) {
		return game.getTileAt(p);
	}

	@Override
	public Unit getUnitAt(Position p) {
		// TODO Auto-generated method stub
		return game.getUnitAt(p);
	}

	@Override
	public City getCityAt(Position p) {
		// TODO Auto-generated method stub
		return game.getCityAt(p);
	}

	@Override
	public Player getPlayerInTurn() {
		// TODO Auto-generated method stub
		return game.getPlayerInTurn();
	}

	@Override
	public Player getWinner() {
		// TODO Auto-generated method stub
		return game.getWinner();
	}

	@Override
	public int getAge() {
		// TODO Auto-generated method stub
		return game.getAge();
	}

	@Override
	public boolean moveUnit(Position from, Position to) {
		boolean validMove = game.moveUnit(from, to);
		if (validMove && transcriptOn) {
			transcript.buildMovementTranscript(from, to, (UnitImpl) getUnitAt(to), getUnitAt(to).getOwner());
		}
		return validMove;
	}

	@Override
	public void endOfTurn() {
		
			transcript.buildEndOFTurn(getPlayerInTurn());
		game.endOfTurn();
	}

	@Override
	public void changeWorkForceFocusInCityAt(Position p, String balance) {
		
			transcript.buildWorkFocusTranscript((CityImpl) getCityAt(p), balance, getCityAt(p).getOwner());
		
		game.changeWorkForceFocusInCityAt(p, balance);
	}

	@Override
	public void changeProductionInCityAt(Position p, String unitType) {
		game.changeProductionInCityAt(p, unitType);
		
			transcript.buildChangeTranscript((CityImpl) getCityAt(p), getCityAt(p).getProduction(), getCityAt(p).getOwner());
		
	}

	@Override
	public void performUnitActionAt(Position p) {
		
			transcript.buildActionTranscript((UnitImpl) getUnitAt(p), getUnitAt(p).getOwner(), 
					((UnitImpl)getUnitAt(p)).getUnitAction(), p);
		
		game.performUnitActionAt(p);
	}
	
	public Transcriber getTranscript() {
		return transcript;
	}
	
	public boolean getState() {
		return transcriptOn;
	}

	public Game toggleDecorator() {
		return game;
	}

	@Override
	public void setTranscriber(boolean on) {
		game.setTranscriber(on);
		
	}

	@Override
	public void addObserver(SwitchView switchView) {
		game.addObserver(switchView);
		
	}

	@Override
	public boolean getTranscriptOn() {
		return game.getTranscriptOn();
	}
}
