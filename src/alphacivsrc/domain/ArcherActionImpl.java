package alphacivsrc.domain;

public class ArcherActionImpl implements UnitActions {

	@Override
	public void unitPreformsAction(UnitContext unitContext) {
		UnitImpl unit = unitContext.getTile().getUnit();
		if(unit.getMoveCount() == 0) {
			unit.setDefensiveStrength(unit.getDefensiveStrength()/2);
			unit.setMoveCount(1);
		}else {
			unit.setDefensiveStrength(unit.getDefensiveStrength()*2);
			unit.setMoveCount(0);
		}
	}

	@Override
	public String getActionName() {
		// TODO Auto-generated method stub
		return "fortified";
	}
}
