package alphacivsrc.domain;

public class SwitchController implements SwitchControllerInterface {
	
	private Game game;	 
	
	//private boolean 
	public SwitchController(Game game) {
		this.game = game;
	}

	public void switchState() {
		this.game = game.toggleDecorator();
	}

	@Override
	public void endTheTurn() {
	     game.endOfTurn();
		
	}
	

}
