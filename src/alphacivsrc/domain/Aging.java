package alphacivsrc.domain;

public interface Aging {
	public int calculateAge(int year);
}
