package alphacivsrc.domain;

public interface UnitCreationFactory {
	public UnitImpl createUnit(Player owner, String unitType);

	public void setUnitAction(String unitType, UnitActions unitAction);
}
