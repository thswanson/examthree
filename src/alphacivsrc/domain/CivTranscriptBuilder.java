package alphacivsrc.domain;

public class CivTranscriptBuilder implements Transcriber {
	private String transcript = "";
	
	@Override
	public String transcriptOut() {
		return transcript;
	}

	@Override
	public void buildMovementTranscript(Position from,Position to, UnitImpl unit, Player player) {
		String temp = player + " moved unit " + unit.getTypeString() +
				" from " + from.getRow() + ","+ from.getColumn() + " to " + to.getRow() + ","+to.getColumn()+"\n";
		System.out.println(temp);
		transcript += temp;
		
	}

	@Override
	public void buildActionTranscript(UnitImpl unit, Player player, UnitActions unitAction, Position pos) {
		String temp =  player + " " + unit.getTypeString() + " " +  unitAction.getActionName()+" at "+pos.getRow()+","+pos.getColumn()+"\n";
		System.out.println(temp);
		transcript += temp;
	}

	@Override
	public void buildBuildTranscript(CityImpl city, Position placed, String unit, Player player) {
		String temp = player + " city at "+city.getPosition().getRow()+","+city.getPosition().getColumn() + " produced " + unit+"\n";
		System.out.println(temp);
		transcript += temp;
	}

	@Override
	public void buildChangeTranscript(CityImpl city, String unit, Player player) {
		// TODO Auto-generated method stub
		String temp =  player + " changes production focus in city at "+city.getPosition().getRow()+","+city.getPosition().getColumn() + " to " + city.getProduction()+"\n";
		System.out.println(temp);
		transcript += temp;
	}
	@Override
	public void buildWorkFocusTranscript(CityImpl city, String focus, Player player) {
		// TODO Auto-generated method stub
		String temp =  player + " changes work focus in city at "+city.getPosition().getRow()+","+city.getPosition().getColumn() + " to " + focus +"\n";
		System.out.println(temp);
		transcript += temp;
	}

	public void buildEndOFTurn(Player playerInTurn) {
		String temp = playerInTurn + " ended the turn\n";
		System.out.println(temp);
		transcript += temp;
		
	}
	
	

}
