package alphacivsrc.domain;

public class SwitchViewOn implements SwitchView {

	SwitchControllerInterface controller;
	Game game;
	boolean state;
	private Player playerInTurn;
	public SwitchViewOn(Game game, SwitchControllerInterface controller) {
		this.game = game;
		this.controller = controller;
		
		//controller = new SwitchController();
	}
	
	@Override
	public void onClickToggleTranscript() {
		controller.switchState();
	} 
	
	
	@Override
	public void notifyFromModel() {
		update();
	}
	
	@Override
	public void update() {
		state = game.getTranscriptOn();
		playerInTurn = game.getPlayerInTurn();
		redraw();
	}

	@Override
	public void redraw() {
		System.out.println("THIS IS THE GUI AND THE GRAPHICS");
		if (state) {
			System.out.println("This button is now on" );
		}
		else {
			System.out.println("This button is now off" );
		} 
		
		System.out.println(playerInTurn);
		
	}
	@Override
	public void endOfTurnButton() {
		controller.endTheTurn();
		
	}
	
	public static void main(String [] args) {
		
		Game game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp(), new UnitCreationAlphaCiv());
		SwitchControllerInterface controller = new SwitchController(game);
		SwitchView switcherz = new SwitchViewOn(game, controller);
		game.addObserver(switcherz);
		switcherz.update();
		switcherz.onClickToggleTranscript();
		//switcherz.update();
		switcherz.endOfTurnButton();
		switcherz.endOfTurnButton();
		switcherz.endOfTurnButton();
		switcherz.endOfTurnButton();
		switcherz.endOfTurnButton();
		
	}

	

}
