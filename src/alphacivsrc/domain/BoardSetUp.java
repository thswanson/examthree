package alphacivsrc.domain;

import java.util.ArrayList;

public interface BoardSetUp {
	public TileImpl[][] setUpBoard(UnitCreationFactory unitFactory);
	public ArrayList<CityImpl> getCities();
	public ArrayList<UnitImpl>  getUnits();
}
