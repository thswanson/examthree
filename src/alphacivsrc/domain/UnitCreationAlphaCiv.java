package alphacivsrc.domain;

public class UnitCreationAlphaCiv implements UnitCreationFactory {

	@Override
	public UnitImpl createUnit(Player owner, String unitType) {
		UnitImpl unit = new UnitImpl(owner,unitType, new NoUnitActionImpl());
		return unit;
	}

	@Override
	public void setUnitAction(String unitType, UnitActions unitAction) {
		// TODO Auto-generated method stub
		
	}

}
