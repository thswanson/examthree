package alphacivsrc.domain;

public interface SwitchView {
	
	public void notifyFromModel();
	
	public void onClickToggleTranscript();
	
	public void endOfTurnButton();
	
	public void update();

	void redraw();


}
