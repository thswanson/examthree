package alphacivsrc.domain;

import java.util.ArrayList;

public class AlphaCivSetUp implements BoardSetUp {
	ArrayList<CityImpl> cities;
	ArrayList<UnitImpl> units;
	
	public AlphaCivSetUp() {
		cities = new ArrayList<CityImpl>();
		units = new ArrayList<UnitImpl>();
	}
	
	@Override
	public ArrayList<CityImpl> getCities() {
		return cities;
	}
	
	@Override
	public ArrayList<UnitImpl>  getUnits() {
		return units;
	}
	
	@Override
	public TileImpl[][] setUpBoard(UnitCreationFactory unitFactory) {
		TileImpl[][] boardArray = new TileImpl[GameConstants.WORLDSIZE][GameConstants.WORLDSIZE];
		for (int row = 0; row < GameConstants.WORLDSIZE; row++) {
			for (int column = 0; column < GameConstants.WORLDSIZE; column++) {
				if (row == 1 && column == 0) {
					TileImpl ocean = new TileImpl(new Position(row, column), GameConstants.OCEANS);
					boardArray[row][column] = ocean;
				} else if (row == 0 && column == 1) {
					TileImpl hill = new TileImpl(new Position(row, column), GameConstants.HILLS);
					boardArray[row][column] = hill;
				} else if (row == 1 && column == 1) {
					TileImpl plain = new TileImpl(new Position(row, column), GameConstants.PLAINS);
					CityImpl city = new CityImpl(Player.RED,new Position(row, column) );
					plain.setCity(city);
					cities.add(city);
					boardArray[row][column] = plain;
				} else if (row == 2 && column == 0) {
					TileImpl plain = new TileImpl(new Position(row, column), GameConstants.PLAINS);
					UnitImpl unit = unitFactory.createUnit(Player.RED, GameConstants.ARCHER);
					plain.setUnit(unit);
					units.add(unit);
					boardArray[row][column] = plain;
				} else if (row == 2 && column == 2) {
					TileImpl mountain = new TileImpl(new Position(row, column), GameConstants.MOUNTAINS);
					boardArray[row][column] = mountain;
				} else if (row == 3 && column == 2) {
					TileImpl plain = new TileImpl(new Position(row, column), GameConstants.PLAINS);
					UnitImpl unit = unitFactory.createUnit(Player.BLUE, GameConstants.LEGION);
					plain.setUnit(unit);
					units.add(unit);
					boardArray[row][column] = plain;
				} else if (row == 4 && column == 3) {
					TileImpl plain = new TileImpl(new Position(row, column), GameConstants.PLAINS);
					UnitImpl unit = unitFactory.createUnit(Player.RED, GameConstants.SETTLER);
					plain.setUnit(unit);
					units.add(unit);
					boardArray[row][column] = plain;
				} else if (row == 4 && column == 1) {
					TileImpl plain = new TileImpl(new Position(row, column), GameConstants.PLAINS);
					CityImpl city = new CityImpl(Player.BLUE,new Position(row, column));
					plain.setCity(city);
					cities.add(city);
					boardArray[row][column] = plain;
				} else {
					TileImpl plain = new TileImpl(new Position(row, column), GameConstants.PLAINS);
					boardArray[row][column] = plain;
				}
			}
		}
		return boardArray;
	}	

}
