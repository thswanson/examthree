package alphacivsrc.domain;

public interface SwitchControllerInterface {
	public void switchState();

	public void endTheTurn();
}
